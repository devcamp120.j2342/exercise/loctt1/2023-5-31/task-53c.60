import models.BigDog;
import models.Cat;
import models.Dog;

public class App {
    public static void main(String[] args) throws Exception {
      Cat cat = new Cat("Myo");
      System.out.println("Cat: ");
        cat.greets();

        System.out.println("----------------------------");

        Dog dog = new Dog("Mực");
        System.out.println("Mực: ");
          dog.greets();
          dog.greets(dog);

          System.out.println("--------------------------------");

          BigDog bigDog = new BigDog("Lu");
          System.out.println("Lu: ");
          bigDog.greets();
          bigDog.greets(bigDog);
          

    }
}
